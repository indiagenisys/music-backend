const status_code = {
  OK: 200,
  CREATED: 201,
  BAD_REQUEST: 400,
  UNAUHTORIZED: 401,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  UNSUPPORTED_MEDIA_TYPE: 415,
  INTERNAL_SERVER_ERROR: 500,
};

const status_type = {
  ERROR: 'error',
  SUCCESS: 'success',
};

const status_message = {
  USERNAME_EXISTS: `Username '#username#' already exists`,
  INVALID: `Invalid username or password`,
  NOT_FOUND: `#entity# not found`,
  VALIDATED: `Token validated`,
  UNAUHTORIZED: `Unauthorized`,
  INTERNAL_SERVER_ERROR: `Internal Server Error`,
  ACTIVED: `#entity# activated!`,
  DEACTIVED: `#entity# deactivated!`,
  CREATE_SUCCESS: `#entity# created successfully!`,
  DELETE_SUCCESS: `#entity# deleted successfully!`,
};

export { status_code, status_type, status_message };
