import { Document } from 'mongoose';
import { Songs } from './songs';

export default interface Playlist extends Document {
    name?: string;
    playlistSongs?: Songs[];
}
