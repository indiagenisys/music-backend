import { Document } from 'mongoose';
import IUser from './user';

export interface RequestSong extends Document {
  title: string;
  description?: string;
  user_id: IUser;
  is_approved: isApproved;
}

export enum isApproved {
  isDeclined = 0,
  isAccepted = 1,
  isPending = 2,
}
