import { Document } from 'mongoose';
import IUser from './user';
import { Songs } from './songs';

export interface FavouriteSongs extends Document {
  songs: Songs[];
  user_id: IUser;
}
