import { Document } from 'mongoose';

export interface Songs extends Document {
  title?: string;
  link?: string;
  artist?: string;
  duration?: number;
  description?: string;
  image?: string;
}
