"use strict";
exports.__esModule = true;
var jsonwebtoken_1 = require("jsonwebtoken");
var config_1 = require("../config/config");
var logging_1 = require("../config/logging");
var NAMESPACE = 'Auth';
var signJWT = function (user, callback) {
    var timeSinceEpoch = new Date().getTime();
    var expirationTime = timeSinceEpoch + Number(config_1["default"].server.token.expireTime) * 100000;
    var expirationTimeInSeconds = Math.floor(expirationTime / 1000);
    logging_1["default"].info(NAMESPACE, "Attempting to sign token for " + user._id);
    try {
        jsonwebtoken_1["default"].sign({
            username: user.username
        }, config_1["default"].server.token.secret, {
            issuer: config_1["default"].server.token.issuer,
            algorithm: 'HS256',
            expiresIn: expirationTimeInSeconds
        }, function (error, token) {
            if (error) {
                callback(error, null);
            }
            else if (token) {
                callback(null, token);
            }
        });
    }
    catch (error) {
        logging_1["default"].error(NAMESPACE, error.message, error);
        callback(error, null);
    }
};
exports["default"] = signJWT;
