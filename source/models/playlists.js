"use strict";
exports.__esModule = true;
var mongoose_1 = require("mongoose");
var mongoose_2 = require("mongoose");
var songs = new mongoose_1.Schema({
    name: { type: String, required: true },
    artist: { type: String, required: true },
    duration: { type: Number, required: true }
});
var PlayListSchema = new mongoose_1.Schema({
    name: { type: String, required: true },
    songs: [songs]
});
exports["default"] = mongoose_2["default"].model('Playlist', PlayListSchema);
