import mongoose, { Schema } from 'mongoose';
import songs from '../controllers/songs';
import { Songs } from '../interfaces';

const SongSchema: Schema = new Schema(
  {
    title: { type: String, required: true, unique: true, text: true },
    link: { type: String, required: true },
    artist: { type: String, required: true },
    duration: { type: Number, required: true },
    description: { type: String, required: false },
    image: { type: String, required: true },
  },
  {
    timestamps: true,
  }
);

SongSchema.index({ title: 'text', artist: 'text' });

export default mongoose.model<Songs>('Songs', SongSchema);
