import mongoose, { Schema } from 'mongoose';
import IUser from '../interfaces/user';

const UserSchema: Schema = new Schema(
  {
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    isActive: { type: Boolean, default: true, required: true },
  },
  {
    timestamps: true,
  }
);

// Hiding internal Data
UserSchema.methods.toJSON = function () {
  const user = this;
  const userObject: any = user.toObject();

  delete userObject.password;
  // delete userObject.createdAt;
  // delete userObject.updatedAt;
  delete userObject.__v;
  return userObject;
};

export default mongoose.model<IUser>('User', UserSchema);
