import mongoose, { Schema } from 'mongoose';
import IAdmin from '../interfaces/user';

const AdminSchema: Schema = new Schema(
  {
    username: { type: String, required: true, index: true, unique: true },
    password: { type: String, required: true },
  },
  {
    timestamps: true,
  }
);

AdminSchema.index({ username: 1 }, { unique: true });

// Hiding Password ans Token Property
AdminSchema.methods.toJSON = function () {
  const admin = this;
  const adminObject: any = admin.toObject();

  // delete adminObject.password;
  delete adminObject.__v;
  delete adminObject.createdAt;
  delete adminObject.updatedAt;

  return adminObject;
};

export default mongoose.model<IAdmin>('Admin', AdminSchema);
