import { Schema } from 'mongoose';
import mongoose from 'mongoose';
import { FavouriteSongs } from '../interfaces/favouriteSongs';

const favouriteSongsSchema: Schema = new Schema(
  {
    songs: [{ type: Schema.Types.ObjectId, ref: 'Songs' }, { unique: true }],
    user_id: { type: Schema.Types.ObjectId, ref: 'User', unique: true },
  },
  {
    timestamps: true,
  }
);

// Hiding internal Data
favouriteSongsSchema.methods.toJSON = function () {
  const favouriteSong = this;
  const favouriteSongObject: any = favouriteSong.toObject();

  delete favouriteSongObject.createdAt;
  delete favouriteSongObject.updatedAt;
  delete favouriteSongObject.__v;
  return favouriteSongObject;
};

export default mongoose.model<FavouriteSongs>(
  'FavouriteSongs',
  favouriteSongsSchema
);
