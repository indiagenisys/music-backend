import { Schema } from 'mongoose';
import mongoose from 'mongoose';
import Playlist from '../interfaces/playlist';

let ObjectId = mongoose.Schema.Types.ObjectId;

const PlayListSchema: Schema = new Schema({
  name: { type: String, required: true },
  playlistSongs: [
    { type: Schema.Types.ObjectId, ref: 'Songs' },
    { unique: true },
  ],
  user_id: { type: Schema.Types.ObjectId, ref: 'User' },
});

export default mongoose.model<Playlist>('Playlist', PlayListSchema);
