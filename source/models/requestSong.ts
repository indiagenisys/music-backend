import { Schema } from 'mongoose';
import mongoose from 'mongoose';
import { isApproved, RequestSong } from '../interfaces/requestSong';

const RequestSongSchema: Schema = new Schema(
  {
    title: { type: String, required: true },
    description: { type: String, required: false },
    user_id: { type: Schema.Types.ObjectId, ref: 'User' },
    is_approved: {
      type: Number,
      required: true,
      default: isApproved.isPending,
    },
  },
  {
    timestamps: true,
  }
);

// Hiding internal Data
RequestSongSchema.methods.toJSON = function () {
  const requestSong = this;
  const requestSongObject: any = requestSong.toObject();

  delete requestSongObject.createdAt;
  delete requestSongObject.updatedAt;
  delete requestSongObject.__v;
  return requestSongObject;
};

export default mongoose.model<RequestSong>('RequestSong', RequestSongSchema);
