import jwt from 'jsonwebtoken';
import config from '../config/config';
import logging from '../config/logging';
import Admin from '../models/admin';
import * as constant from '../config/constant';
import { Request, Response, NextFunction } from 'express';

const NAMESPACE = 'Auth';

const extractAdminJWT = (req: any, res: Response, next: NextFunction) => {
  logging.info(NAMESPACE, 'Validating token');

  let token = req.headers.authorization?.split(' ')[1];

  if (token) {
    jwt.verify(token, config.server.token.secret, (error: any, decoded: any) => {
      if (error) {
        return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
          data: {
            error: constant.status_message.INTERNAL_SERVER_ERROR,
          },
          status_code: constant.status_code.INTERNAL_SERVER_ERROR,
          status: constant.status_type.ERROR,
        });
      } else {
        const admin = Admin.findOne({
          username: decoded.user.username,
        });
        if (!decoded || !admin) {
          return res.status(constant.status_code.UNAUHTORIZED).send({
            data: {
              error: constant.status_message.UNAUHTORIZED,
            },
            status_code: constant.status_code.UNAUHTORIZED,
            status: constant.status_type.ERROR,
          });
        }
        res.locals.jwt = decoded;
        req.user = decoded.user;
        next();
      }
    });
  } else {
    return res.status(401).json({
      data: {
        error: constant.status_message.UNAUHTORIZED,
      },
      status_code: constant.status_code.UNAUHTORIZED,
      status: constant.status_type.ERROR,
    });
  }
};

export default extractAdminJWT;
