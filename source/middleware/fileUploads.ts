import * as buffer from 'buffer';
import * as fs from 'fs';
import * as path from 'path';

const storeImage = async (base64str: any, filename: any, filepath: any) => {

    let buf = buffer.Buffer.from(base64str, 'base64');
    fs.writeFile(
      path.resolve("/" + filepath + "/" + filename).substring(3),
      buf,
      (error) => {
        if (error) {
          throw error;
        }
      }
    );

    return filename;
};

export default storeImage;
