import { NextFunction, Request, Response } from 'express';
import bcryptjs from 'bcryptjs';
import logging from '../config/logging';
import User from '../models/user';
import RequestSong from '../models/requestSong';
import FavouriteSongs from '../models/favouriteSongs';
import * as _ from 'lodash';
import signJWT from '../functions/signJTW';
import * as constant from '../config/constant';

const NAMESPACE = 'User';

const validateToken = (req: Request, res: Response, next: NextFunction) => {
  logging.info(NAMESPACE, 'Token validated, user authorized.');

  return res.status(constant.status_code.OK).send({
    status_code: constant.status_code.OK,
    status: constant.status_type.SUCCESS,
    data: {
      success: constant.status_message.VALIDATED,
    },
  });
};

const register = async (req: Request, res: Response, next: NextFunction) => {
  let { username, password } = req.body;
  try {
    const userExist = await User.find({
      username: { $regex: '^' + username + '$', $options: 'i' },
    });
    if (userExist.length > 0) {
      return res.status(constant.status_code.BAD_REQUEST).send({
        status: constant.status_type.ERROR,
        status_code: constant.status_code.BAD_REQUEST,
        data: {
          error: constant.status_message.USERNAME_EXISTS.replace(
            '#username#',
            username
          ),
        },
      });
    }

    await bcryptjs.hash(password, 10, async (hashError, hash) => {
      if (hashError) {
        return res.status(constant.status_code.BAD_REQUEST).send({
          status: constant.status_type.ERROR,
          status_code: constant.status_code.BAD_REQUEST,
          data: {
            error: hashError,
          },
        });
      }

      const user = await new User({
        username,
        password: hash,
      });

      await user.save();

      res.status(constant.status_code.CREATED).send({
        status: constant.status_type.SUCCESS,
        status_code: constant.status_code.CREATED,
        data: {
          user,
        },
      });
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

const login = async (req: Request, res: Response, next: NextFunction) => {
  let { username, password } = await req.body;

  try {
    const user = await User.findOne({
      username: { $regex: '^' + username + '$', $options: 'i' },
      isActive: true
    });
    if (!user) {
      return res.status(constant.status_code.UNAUHTORIZED).send({
        status: constant.status_type.ERROR,
        status_code: constant.status_code.UNAUHTORIZED,
        data: {
          error: constant.status_message.INVALID,
        },
      });
    }

    const password_match = await bcryptjs.compare(password, user.password);
    if (!password_match) {
      return res.status(constant.status_code.UNAUHTORIZED).send({
        status: constant.status_type.ERROR,
        status_code: constant.status_code.UNAUHTORIZED,
        data: {
          error: constant.status_message.INVALID,
        },
      });
    }

    await signJWT(user, async (error, token) => {
      if (error) {
        return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
          status: constant.status_type.ERROR,
          status_code: constant.status_code.INTERNAL_SERVER_ERROR,
          data: {
            error: error.message,
          },
        });
      } else if (token) {
        return res.status(constant.status_code.OK).send({
          status: constant.status_type.SUCCESS,
          status_code: constant.status_code.OK,
          data: {
            token,
            user,
          },
        });
      }
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

const requestSongs = async (req: any, res: Response, next: NextFunction) => {
  let { title, description, user_id } = req.body;
  try {
    const user = await User.findById(user_id);
    if (!user) {
      return res.status(constant.status_code.NOT_FOUND).send({
        status: constant.status_type.ERROR,
        status_code: constant.status_code.NOT_FOUND,
        data: constant.status_message.NOT_FOUND.replace('#entity#', 'User'),
      });
    }
    const requestedSong = await new RequestSong({
      title,
      description,
      user_id: user_id,
      is_approved: 2,
    }).populate('user');
    await requestedSong.save();
    res.status(constant.status_code.CREATED).send({
      status: constant.status_type.SUCCESS,
      status_code: constant.status_code.CREATED,
      data: {
        requestedSong: requestedSong,
      },
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

const addFavouriteSongs = async (
  req: any,
  res: Response,
  next: NextFunction
) => {
  let { user_id, songs } = req.body;
  try {
    const existFavourite = await FavouriteSongs.findOne().populate({
      path: 'user_id',
      match: {
        _id: user_id,
      },
    });
    let favouriteSongs: any = {};
    if (!existFavourite) {
      favouriteSongs = await new FavouriteSongs({
        user_id,
        songs,
      });
      await favouriteSongs.save();
    } else {
      if (songs) {
        if (!existFavourite.songs || existFavourite.songs.length === 0) {
          existFavourite['songs'] = songs;
        } else {
          songs.forEach(async (element: any) => {
            if (existFavourite['songs'].indexOf(element) === -1) {
              await existFavourite['songs'].push(element);
            }
          });
        }
        await existFavourite.save();
      }
      favouriteSongs = await FavouriteSongs.findOne().populate({
        path: 'user_id',
        match: {
          _id: user_id,
        },
      });
    }
    return res.status(constant.status_code.OK).send({
      status: constant.status_type.SUCCESS,
      status_code: constant.status_code.OK,
      data: {
        favouriteSongs,
      },
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

const getFavouriteSongs = async (
  req: any,
  res: Response,
  next: NextFunction
) => {
  const user_id = req.query.id;
  try {
    const favouriteSongs = await FavouriteSongs.findOne()
      .populate({
        path: 'user_id',
        match: {
          _id: user_id,
        },
      })
      .populate('song');
    return res.status(constant.status_code.OK).send({
      status_code: constant.status_code.OK,
      status: constant.status_type.SUCCESS,
      data: {
        favouriteSongs,
      },
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

const removeFavouriteSongs = async (
  req: any,
  res: Response,
  next: NextFunction
) => {
  const user_id = req.query.id;
  const songs: any = req.body.songs;
  try {
    const favouriteSongs: any = await FavouriteSongs.findOne().populate({
      path: 'user_id',
      match: {
        _id: user_id,
      },
    });

    if (!favouriteSongs) {
      return res.status(constant.status_code.NOT_FOUND).send({
        status: constant.status_type.ERROR,
        status_code: constant.status_code.NOT_FOUND,
        data: {
          error: constant.status_message.NOT_FOUND.replace('#entity#', 'Song'),
        },
      });
    }

    songs.forEach(async (element: any) => {
      if (favouriteSongs['songs'] && favouriteSongs['songs'].length > 0) {
        if (favouriteSongs['songs'].indexOf(element) !== -1) {
          await favouriteSongs['songs'].pop(element);
        }
      }
    });
    await favouriteSongs.save();

    const updatedFavourites = await FavouriteSongs.findOne().populate({
      path: 'user_id',
      match: {
        _id: user_id,
      },
    });
    res.status(constant.status_code.OK).send({
      status: constant.status_type.SUCCESS,
      status_code: constant.status_code.OK,
      data: {
        favouriteSongs: updatedFavourites,
      },
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

export default {
  validateToken,
  register,
  login,
  requestSongs,
  addFavouriteSongs,
  getFavouriteSongs,
  removeFavouriteSongs,
};