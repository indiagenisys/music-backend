"use strict";
exports.__esModule = true;
var playlists_1 = require("../models/playlists");
var mongoose_1 = require("mongoose");
var NAMESPACE = 'Requestsongs';
var addPlaylists = function (req, res, next) {
    var _a = req.body, name = _a.name, songs = _a.songs;
    console.log('request', req.body);
    var _playlist = new playlists_1["default"]({
        _id: new mongoose_1["default"].Types.ObjectId(),
        name: name,
        songs: songs
    });
    return _playlist
        .save()
        .then(function (playList) {
        return res.status(201).json({ playList: playList });
    })["catch"](function (error) {
        return res.status(500).json({
            message: error.message,
            error: error
        });
    });
};
var getAllPlaylists = function (req, res, next) {
    playlists_1["default"]
        .find()
        .select('id')
        .exec()
        .then(function (playlists) {
        return res.status(200).json({
            users: playlists,
            count: playlists.length
        });
    })["catch"](function (error) {
        return res.status(500).json({
            message: error.message,
            error: error
        });
    });
    return;
};
exports["default"] = { addPlaylists: addPlaylists, getAllPlaylists: getAllPlaylists };
