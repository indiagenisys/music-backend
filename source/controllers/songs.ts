import { Request, Response, NextFunction } from 'express';
import Songs from '../models/songs';
import * as constant from '../config/constant';

const allSongs = async (req: Request, res: Response, next: NextFunction) => {
  let query = req.query as any;
  let searchQuery: any = {};
  if (query.hasOwnProperty('search')) {
    searchQuery = {
      $or: [
        {
          title: { $regex: '^' + query.search, $options: 'i' },
        },
        {
          artist: { $regex: query.search, $options: 'i' },
        },
      ],
    };
  }
  let limit = 10;
  let offset = 0;
  if (query.hasOwnProperty('limit')) {
    limit = parseInt(query.limit);
  }
  if (query.hasOwnProperty('offset')) {
    offset = parseInt(query.offset);
  }
  
  const facetData = [
    {
      $skip: offset,
    },
    {
      $limit: limit,
    },
    {
      $sort: { title: -1 },
    },
  ];

  try {
    const songs: any = await Songs.aggregate([
      {
        $match: searchQuery,
      },
      {
        $facet: {
          data: facetData,
          count: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);

    const newData: any = {};
    newData.songs = songs[0].data;
    if (songs[0].count.length) {
      newData.count = songs[0].count[0].count;
    } else {
      newData.count = 0;
    }

    res.status(constant.status_code.OK).send({
      status: constant.status_type.SUCCESS,
      status_code: constant.status_code.OK,
      data: newData,
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

const getSong = async (req: Request, res: Response, next: NextFunction) => {
  const id = req.params.id;
  try{
    const song = await Songs.findById(id);

    if(!song){
      return res.status(constant.status_code.NOT_FOUND).send({
        status: constant.status_type.ERROR,
        status_code: constant.status_code.NOT_FOUND,
        data: {
          error: constant.status_message.NOT_FOUND.replace('#entity#', 'Song'),
        },
      });  
    }

    res.status(constant.status_code.OK).send({
      status: constant.status_type.SUCCESS,
      status_code: constant.status_code.OK,
      data: {
        song
      },
    });
  }catch(error){
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

const addSong = async (req: Request, res: Response, next: NextFunction) => {
  const { title, link, artist, duration, description, image } = req.body;
  try {
    let song;
    if(req.body._id){
      song = await Songs.findByIdAndUpdate(req.body._id, {
        title,
        link,
        artist,
        duration,
        description,
        image,
      }, {
        new: true
      });
    }else{
      song = await new Songs({
        title,
        link,
        artist,
        duration,
        description,
        image,
      });
      await song.save();
    }
    res.status(constant.status_code.OK).send({
      status: constant.status_type.SUCCESS,
      status_code: constant.status_code.OK,
      data: {
        song,
      },
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

const deleteSong = async (req: Request, res: Response, next: NextFunction) => {
  try{
    const id = req.params.id;
    await Songs.findOneAndDelete({ _id: id});
    res.status(constant.status_code.OK).send({
      status: constant.status_type.SUCCESS,
      status_code: constant.status_code.OK,
      data: {
        message: constant.status_message.DELETE_SUCCESS.replace(
          '#entity#',
          'Song'
        ),
      },
    });
  }catch(error){
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }

}

export default { getSong, allSongs, addSong, deleteSong };
