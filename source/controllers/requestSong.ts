import { Response, NextFunction, Request } from 'express';
import RequestSong from '../models/requestSong';
import User from '../models/user';
import * as constant from '../config/constant';

const NAMESPACE = 'RequestSong';

const requestSongs = async (req: any, res: Response, next: NextFunction) => {
  let { title, description, user_id } = req.body;
  try {
    const user = await User.findById(user_id);
    if (!user) {
      return res.status(constant.status_code.NOT_FOUND).send({
        status: constant.status_type.ERROR,
        status_code: constant.status_code.NOT_FOUND,
        data: constant.status_message.NOT_FOUND.replace('#entity#', 'User'),
      });
    }
    const requestedSong = await new RequestSong({
      title,
      description,
      user_id: user_id,
      is_approved: 2,
    }).populate('user');
    await requestedSong.save();
    res.status(constant.status_code.CREATED).send({
      status: constant.status_type.SUCCESS,
      status_code: constant.status_code.CREATED,
      data: {
        requestedSong: requestedSong,
      },
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

export default { requestSongs };
