import { Response, NextFunction, Request } from 'express';
import Playlists from '../models/playlists';
import * as constant from '../config/constant';

const NAMESPACE = 'Playlist';

const addPlaylists = async (req: any, res: Response, next: NextFunction) => {
  let { name, playlistSongs } = req.body;
  const user_id = req.user._id;
  try {
    const _playlist = await new Playlists({
      name,
      playlistSongs,
      user_id,
    });

    await _playlist.save();

    await res.status(constant.status_code.CREATED).send({
      status: constant.status_type.SUCCESS,
      status_code: constant.status_code.CREATED,
      data: {
        playlist: _playlist,
      },
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

const updatePlaylist = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const playlist: any = await Playlists.findById(req.params.id);

    if (req.body.name) {
      playlist.name = req.body.name;
    }
    if (req.body.playlistSongs) {
      if (!playlist.playlistSongs || playlist.playlistSongs.length === 0) {
        playlist['playlistSongs'] = req.body.playlistSongs;
      } else {
        req.body.playlistSongs.forEach((element: any) => {
          if (playlist['playlistSongs'].indexOf(element) === -1) {
            playlist['playlistSongs'].push(element);
          }
        });
      }
    }

    await playlist.update(playlist);

    const newPlaylists = await Playlists.findOne({
      _id: req.params.id,
    }).populate('playlistSongs');

    return res.status(constant.status_code.OK).send({
      status: constant.status_type.SUCCESS,
      status_code: constant.status_code.OK,
      data: {
        playlists: newPlaylists,
      },
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

const removeSongFromPlaylist = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let id = req.params.id;
  try {
    const playlist = await Playlists.findById(id);
    if (!playlist) {
      return res.status(constant.status_code.NOT_FOUND).send({
        status: constant.status_type.ERROR,
        status_code: constant.status_code.NOT_FOUND,
        data: {
          error: constant.status_message.NOT_FOUND.replace(
            '#entity#',
            'Playlist'
          ),
        },
      });
    }
    await playlist.update({
      $pull: { playlistSongs: { $in: req.body.playlistSongs } },
    });

    await playlist.save();
    const updatedPlaylist = await Playlists.findById(id).populate(
      'playlistSongs'
    );
    res.status(constant.status_code.OK).send({
      status: constant.status_type.SUCCESS,
      status_code: constant.status_code.OK,
      data: {
        playlist: updatedPlaylist,
      },
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

const removePlaylist = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const id = req.params.id;
    const playlist = await Playlists.findById(id);
    if (!playlist) {
      return res.status(constant.status_code.NOT_FOUND).send({
        status: constant.status_type.ERROR,
        status_code: constant.status_code.NOT_FOUND,
        data: {
          error: constant.status_message.NOT_FOUND.replace(
            '#entity#',
            'Playlist'
          ),
        },
      });
    }

    await playlist.delete();
    res.status(constant.status_code.OK).send({
      status: constant.status_type.SUCCESS,
      status_code: constant.status_code.OK,
      data: {
        message: constant.status_message.DELETE_SUCCESS.replace(
          '#entity#',
          'Playlist'
        ),
      },
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

const getAllPlaylists = async (req: any, res: Response, next: NextFunction) => {
  try {
    const user_id = req.user._id;
    const playlists = await Playlists.find({
      user_id,
    }).populate('playlistSongs');
    res.status(constant.status_code.OK).send({
      status: constant.status_type.SUCCESS,
      status_code: constant.status_code.OK,
      data: {
        playlists,
        count: playlists.length,
      },
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

export default {
  addPlaylists,
  getAllPlaylists,
  updatePlaylist,
  removeSongFromPlaylist,
  removePlaylist,
};
