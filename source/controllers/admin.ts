import Songs from '../models/songs';
import { NextFunction, Request, Response } from 'express';
import bcryptjs from 'bcryptjs';
import Admin from '../models/admin';
import User from '../models/user';
import RequestSong from '../models/requestSong';
import * as _ from 'lodash';
import signJWT from '../functions/signJTW';
import * as constant from '../config/constant';
import * as fs from 'fs';
import * as mm from 'music-metadata';
import storeImage from '../middleware/fileUploads';
import config from '../config/config';
// import storeFiles from '../functions/storeFiles';
const NAMESPACE = 'Admin';

const register = async (req: Request, res: Response, next: NextFunction) => {
  let { username, password } = await req.body;
  // return res.send(constant);
  const adminExist: any = await Admin.find({
    username: { $regex: '^' + username + '$', $options: 'i' },
  });
  if (adminExist.length > 0) {
    return res.status(constant.status_code.BAD_REQUEST).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.BAD_REQUEST,
      data: {
        error: constant.status_message.USERNAME_EXISTS.replace(
          '#username#',
          username
        ),
      },
    });
  }

  await bcryptjs.hash(password, 10, async (hashError, hash) => {
    if (hashError) {
      return res.status(constant.status_code.UNAUHTORIZED).send({
        status: constant.status_type.ERROR,
        status_code: constant.status_code.UNAUHTORIZED,
        data: {
          error: hashError,
        },
      });
    }

    const admin = await new Admin({
      username,
      password: hash,
    });

    try {
      await admin.save();
      res.status(constant.status_code.CREATED).send({
        status: constant.status_type.SUCCESS,
        status_code: constant.status_code.CREATED,
        data: {
          admin,
        },
      });
    } catch (error) {
      return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
        status: constant.status_type.ERROR,
        status_code: constant.status_code.INTERNAL_SERVER_ERROR,
        data: error.message,
      });
    }
  });
};

const login = async (req: Request, res: Response, next: NextFunction) => {
  let { username, password } = await req.body;

  const admin: any = await Admin.findOne({
    username: { $regex: '^' + username + '$', $options: 'i' },
  });
  try {
    if (!admin) {
      return res.status(constant.status_code.UNAUHTORIZED).send({
        status: constant.status_type.ERROR,
        status_code: constant.status_code.UNAUHTORIZED,
        data: {
          error: constant.status_message.INVALID,
        },
      });
    }

    const password_match = await bcryptjs.compare(password, admin.password);
    if (!password_match) {
      return res.status(constant.status_code.UNAUHTORIZED).send({
        status: constant.status_type.ERROR,
        status_code: constant.status_code.UNAUHTORIZED,
        data: {
          error: constant.status_message.INVALID,
        },
      });
    }

    await signJWT(admin, (error, token) => {
      if (error) {
        return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
          status: constant.status_type.ERROR,
          status_code: constant.status_code.INTERNAL_SERVER_ERROR,
          data: error.message,
        });
      } else if (token) {
        return res.status(constant.status_code.OK).send({
          status: constant.status_type.SUCCESS,
          status_code: constant.status_code.OK,
          data: {
            token,
            admin,
          },
        });
      }
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

const getAllUsers = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const match: any = {};
    if (!req.query.all) {
      match.isActive = true;
    }
    const users = await User.find(match);
    return res.status(constant.status_code.OK).send({
      status_code: constant.status_code.OK,
      data: {
        users,
        count: users.length,
      },
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

const getAllRequestedSongs = async (
  req: any,
  res: Response,
  next: NextFunction
) => {
  const match: any = {};
  if (req.query.title) {
    match.title = { $regex: '^' + req.query.title, $options: 'i' };
  }
  if (req.query.is_approved) {
    match.is_approved = req.query.is_approved;
  }

  try {
    const requestedSongs: any = await RequestSong.find(match)
      .limit(parseInt(req.query.limit))
      .skip(parseInt(req.query.skip))
      .populate('user_id');

    res.send({
      status: constant.status_type.SUCCESS,
      status_code: constant.status_code.OK,
      data: {
        requestedSongs,
      },
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

const updateApproval = async (req: any, res: Response, next: NextFunction) => {
  try {
    const requestedSong = await RequestSong.findById(req.params.id);
    if (!requestedSong) {
      return res.status(constant.status_code.NOT_FOUND).send({
        status: constant.status_type.ERROR,
        status_code: constant.status_code.NOT_FOUND,
        data: {
          error: constant.status_message.NOT_FOUND.replace(
            '#entity#',
            'Requested song'
          ),
        },
      });
    }

    await requestedSong.update({
      is_approved: req.body.is_approved,
    });
    const updatesRequestedSong = await RequestSong.findById(
      req.params.id
    ).populate('user_id');

    res.send({
      status: constant.status_type.SUCCESS,
      status_code: constant.status_code.OK,
      data: {
        requestedSong: updatesRequestedSong,
      },
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
}

const registerUser = async (req: Request, res: Response, next: NextFunction) => {
  let { username, password } = req.body;
  try {
    const userExist = await User.find({
      username: { $regex: '^' + username + '$', $options: 'i' },
    });
    if (userExist.length > 0) {
      return res.status(constant.status_code.BAD_REQUEST).send({
        status: constant.status_type.ERROR,
        status_code: constant.status_code.BAD_REQUEST,
        data: {
          error: constant.status_message.USERNAME_EXISTS.replace(
            '#username#',
            username
          ),
        },
      });
    }

    await bcryptjs.hash(password, 10, async (hashError, hash) => {
      if (hashError) {
        return res.status(constant.status_code.BAD_REQUEST).send({
          status: constant.status_type.ERROR,
          status_code: constant.status_code.BAD_REQUEST,
          data: {
            error: hashError,
          },
        });
      }

      const user = await new User({
        username,
        password: hash,
      });

      await user.save();

      res.status(constant.status_code.CREATED).send({
        status: constant.status_type.SUCCESS,
        status_code: constant.status_code.CREATED,
        data: {
          user,
          message: constant.status_message.CREATE_SUCCESS.replace(
            '#entity#',
            'User'
          ),
        },
      });
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

const toggleActive = async (req: any, res: Response, next: NextFunction) => {
  try {
    const _id = req.params.id;
    const user: any = await User.findById(_id);
    if (!user) {
      return res.status(constant.status_code.NOT_FOUND).send({
        status: constant.status_type.ERROR,
        status_code: constant.status_code.NOT_FOUND,
        data: {
          error: constant.status_message.NOT_FOUND.replace('#entity#', 'User'),
        },
      });
    }
    const active = user.isActive;
    await user.update({ isActive: !user.isActive });
    let message;
    if(active === true){
      message = constant.status_message.DEACTIVED.replace(
        '#entity#',
        'User'
      );
    }else{
      message = constant.status_message.ACTIVED.replace('#entity#', 'User');
    }
    res.send({
      status: constant.status_type.SUCCESS,
      status_code: constant.status_code.OK,
      data: {
        message: message,
      },
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

let dataArr: any = [];

const addBulkSongs= async (req: any, res: Response, next: NextFunction) => {
  try {
    const songpath = req.body.songPath;
    const imagepath = req.body.imagePath;
    const allSongs = await Songs.find();

    let arrayOfFiles = [];
    arrayOfFiles = fs.readdirSync(songpath);

    let data = [];
    let dataToStore: any[] = [];

    data = await storeFiles(arrayOfFiles, songpath, imagepath);
    dataArr = [];
    data.forEach((item: any) => {
      let found = allSongs.findIndex((element: any) => element.title == item.title);

      if (found === -1) {
        dataToStore.push(item);
      }
    });

    if(!dataToStore.length){
      return res.send({
        status: constant.status_type.SUCCESS,
        status_code: constant.status_code.OK,
        data: {
          message: "Song list is already up to date!",
        },
      });
    }
    await Songs.create(dataToStore);
    res.send({
    status: constant.status_type.SUCCESS,
    status_code: constant.status_code.CREATED,
    data: {
      songs: dataToStore,
      },
    });
  } catch (error) {
    return res.status(constant.status_code.INTERNAL_SERVER_ERROR).send({
      status: constant.status_type.ERROR,
      status_code: constant.status_code.INTERNAL_SERVER_ERROR,
      data: {
        error: error.message,
      },
    });
  }
};

const storeFiles: any = async (audioFiles: any, songpath: any, imagepath: any) => {
  try{
    const audioFile = await audioFiles.shift();
    if (audioFile) {
      return mm.parseFile(songpath +"/"+ audioFile).then(async (metadata: any) => {
        let data: any = {};
        data.title = metadata.common.title
          .split(/[`!@#$%^&*()_+\=\[\]{};':"\\|,.<>\/?~]/)[0]
          .trim();
        data.image = imagepath + '/no_song.jpeg';
        let image = await mm.selectCover(metadata.common.picture);
        if(image){
          let filename = data.title + '.' + image?.format.split('/')[1];
          let uploadedImage = await storeImage(
            image?.data,
            filename.replace(/\s/g, '_'),
            imagepath
          );
          data.image = imagepath.split('www').pop() + '/' + uploadedImage;
        }
        data.link = songpath.split('www').pop() + '/' + audioFile;
        data.artist = metadata.common.artist
          ?.toString()
          .split(/[`!@#$%^&*()_+\-=\[\]{};':"\\|.<>\/?~]/)[0]
          .trim();;
        data.duration = parseInt(metadata.format.duration?.toString());
        await dataArr.push(data);
        return storeFiles(audioFiles, songpath, imagepath);
      });
    }
    return dataArr;
  }catch(error){
    throw Error("Files Not uploaded");
  }

}

export default {
  register,
  login,
  getAllUsers,
  getAllRequestedSongs,
  updateApproval,
  registerUser,
  toggleActive,
  addBulkSongs,
};
