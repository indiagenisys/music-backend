"use strict";
exports.__esModule = true;
var http_1 = require("http");
var body_parser_1 = require("body-parser");
var express_1 = require("express");
var logging_1 = require("./config/logging");
var config_1 = require("./config/config");
var user_1 = require("./routes/user");
var playlist_1 = require("./routes/playlist");
var mongoose_1 = require("mongoose");
var cors_1 = require("cors");
var NAMESPACE = 'Server';
var router = express_1["default"]();
/** Connect to Mongo */
mongoose_1["default"]
    .connect(config_1["default"].mongo.url, config_1["default"].mongo.options)
    .then(function (result) {
    logging_1["default"].info(NAMESPACE, 'Mongo Connected');
})["catch"](function (error) {
    logging_1["default"].error(NAMESPACE, error.message, error);
});
/** Log the request */
router.use(function (req, res, next) {
    /** Log the req */
    logging_1["default"].info(NAMESPACE, "METHOD: [" + req.method + "] - URL: [" + req.url + "] - IP: [" + req.socket.remoteAddress + "]");
    res.on('finish', function () {
        /** Log the res */
        logging_1["default"].info(NAMESPACE, "METHOD: [" + req.method + "] - URL: [" + req.url + "] - STATUS: [" + res.statusCode + "] - IP: [" + req.socket.remoteAddress + "]");
    });
    next();
});
/** Parse the body of the request */
router.use(body_parser_1["default"].urlencoded({ extended: true }));
router.use(body_parser_1["default"].json());
router.use(cors_1["default"]());
/** Rules of our API */
router.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    if (req.method == 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
});
/** Routes go here */
router.use('/users', user_1["default"]);
router.use('/playlist', playlist_1["default"]);
/** Error handling */
router.use(function (req, res, next) {
    var error = new Error('Not found');
    res.status(404).json({
        message: error.message
    });
});
var httpServer = http_1["default"].createServer(router);
httpServer.listen(config_1["default"].server.port, function () { return logging_1["default"].info(NAMESPACE, "Server is running " + config_1["default"].server.hostname + ":" + config_1["default"].server.port); });
