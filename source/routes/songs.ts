import express from 'express';
import controller from '../controllers/songs';
import extractJWT from '../middleware/extractJWT';
import extractAdminJWT from '../middleware/extractAdminJWT';

const router = express.Router();

router.get('/allsongs', extractJWT, controller.allSongs);
router.get('/:id', extractJWT, controller.getSong);
router.post('/addsong/:id?', extractAdminJWT, controller.addSong);
router.delete('/deletesong/:id', extractAdminJWT, controller.deleteSong);

export = router;
