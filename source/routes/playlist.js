"use strict";
var express_1 = require("express");
var playlists_1 = require("../controllers/playlists");
var extractJWT_1 = require("../middleware/extractJWT");
var router = express_1["default"].Router();
router.post('/addplaylist', extractJWT_1["default"], playlists_1["default"].addPlaylists);
router.get('/playlist/:id', extractJWT_1["default"], playlists_1["default"].getAllPlaylists);
module.exports = router;
