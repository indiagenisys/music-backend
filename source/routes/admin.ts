import express from 'express';
import controller from '../controllers/admin';
import extractAdminJWT from '../middleware/extractAdminJWT';

const router = express.Router();

// auth
router.post('/register', controller.register);
router.post('/login', controller.login);

// user
router.get('/users', extractAdminJWT, controller.getAllUsers);
router.put('/users/:id', extractAdminJWT, controller.toggleActive);
router.post('/users/register', extractAdminJWT, controller.registerUser);

// songs
router.post('/songs', extractAdminJWT, controller.addBulkSongs);

// requested songs
router.get('/requestedSongs', extractAdminJWT, controller.getAllRequestedSongs);
router.put('/requestedSong/:id', extractAdminJWT, controller.updateApproval);

export = router;
