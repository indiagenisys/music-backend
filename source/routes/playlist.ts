import express from 'express';
import controller from '../controllers/playlists';
import extractJWT from '../middleware/extractJWT';

const router = express.Router();

router.post('/addplaylist', extractJWT, controller.addPlaylists);
router.get('/allplaylist', extractJWT, controller.getAllPlaylists);
router.delete('/:id', extractJWT, controller.removePlaylist);
router.put('/updateplaylist/:id', extractJWT, controller.updatePlaylist);

export = router;
