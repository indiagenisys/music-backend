import express from 'express';
import userController from '../controllers/user';
import extractJWT from '../middleware/extractJWT';

const router = express.Router();

router.get('/validate', extractJWT, userController.validateToken);
router.post('/login', userController.login);
router.post('/requestsong', extractJWT, userController.requestSongs);
router.post('/favouritesongs', extractJWT, userController.addFavouriteSongs);
router.get('/favouritesongs/:id', extractJWT, userController.getFavouriteSongs);
router.put('/favouritesongs/:id', extractJWT, userController.removeFavouriteSongs);

export = router;
