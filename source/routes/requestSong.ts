import express from 'express';
import controller from '../controllers/requestSong';
import extractJWT from '../middleware/extractJWT';

const router = express.Router();

router.post('/requestsong', extractJWT, controller.requestSongs);

export = router;
